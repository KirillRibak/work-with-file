import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;


public class FileModifier {

    public static void editFile(String path) {

        Properties properties = new Properties();
        Properties newProperties = new Properties();


        try (FileInputStream fis = new FileInputStream(path)) {

            properties.load(fis);

            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                String stringKey = entry.getKey().toString();
                String stringValue = entry.getValue().toString();

                stringKey = stringKey.toLowerCase().replace("_", ".");

                newProperties.setProperty(stringKey, stringValue);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fos = new FileOutputStream(path)) {
            newProperties.store(fos, "application.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
