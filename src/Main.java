import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String path;
        boolean condition = true;
        Scanner in = new Scanner(System.in);

        while (condition) {

            System.out.println("Input path:");
            path = in.next();

            if (ValidatingData.checkFile(path)) {
                FileModifier.editFile(path);
                condition = false;
            } else {
                System.out.println("Invalid path");
                condition = true;
            }
        }
    }
}

